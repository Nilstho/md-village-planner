document.addEventListener("DOMContentLoaded", function () {
    const villageContainer = document.getElementById("village-container");
    let selectedIcon = "img/house.png"; // Default selected icon
    let mode = "place"; // Default mode is "Place Icon"
    let selectedIconElement = null; // Keep track of the selected icon


    window.addEventListener("beforeunload", function () {
        localStorage.removeItem("iconPositions");
    });
    // Event listener for button clicks in the button group
    const iconButtons = document.querySelectorAll('.btn-group .btn');
    iconButtons.forEach(button => {
        button.addEventListener('click', function () {
            selectedIcon = this.getAttribute('data-icon');
            // Update other logic based on the selected icon if needed
        });
    });

    // Background image uploader
    const backgroundUpload = document.getElementById("background-upload");

    function loadDefaultBackground() {
        const defaultBackground = 'img/base.jpg';

        // Check if the default background image exists
        fetch(defaultBackground, { method: 'HEAD' })
            .then(response => {
                if (response.ok) {
                    villageContainer.style.backgroundImage = `url(${defaultBackground})`;
                }
            })
            .catch(error => {
                // Handle the error if the image doesn't exist or there's an issue loading it
                console.error('Error loading default background:', error);
            });
    }

    // Load default background when the script is executed
    loadDefaultBackground();

    // Event listener for background image upload
    backgroundUpload.addEventListener("change", function (e) {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function (event) {
                villageContainer.style.backgroundImage = `url(${event.target.result})`;
            };
            reader.readAsDataURL(file);
        }
    });

    // Mode switch buttons
    const placeModeButton = document.getElementById("place-mode");
    const moveRemoveModeButton = document.getElementById("move-remove-mode");

    placeModeButton.addEventListener("click", function () {
        mode = "place";
        placeModeButton.classList.add("active");
        moveRemoveModeButton.classList.remove("active");
    });

    moveRemoveModeButton.addEventListener("click", function () {
        mode = "move-remove";
        moveRemoveModeButton.classList.add("active");
        placeModeButton.classList.remove("active");
    });

    villageContainer.addEventListener("click", handleVillageContainerClick);

    // Update handleVillageContainerClick to handle icon selection and placement/moving
    function handleVillageContainerClick(e) {
        if (mode === "place") {
            addIcon(e);
        } else if (mode === "move-remove") {
            const clickedIcon = e.target.closest(".icon");
            if (clickedIcon) {
                selectedIconElement = clickedIcon;
                console.log("Clicked icon:", clickedIcon);
                highlightSelectedIcon();
            } else {
                selectedIconElement = null;
                removeHighlight();
                // Set a default icon when no icon is selected
                selectedIcon = "img/house.png"; // Replace with the path to your default icon
            }
        }
    }
    


    // Add function to highlight the selected icon
    function highlightSelectedIcon() {
        removeHighlight(); // Clear previous highlight
        selectedIconElement.classList.add("selected");
    }

    // Add function to remove highlight from all icons
    function removeHighlight() {
        const icons = document.querySelectorAll(".icon");
        icons.forEach(icon => icon.classList.remove("selected"));
    }

    // Update handleIconMouseMove to move the selected icon only
    function handleIconMouseMove(e) {
        // Move the selected icon if the user is dragging it and the mode is "move-remove"
        if (selectedIconElement && mode === "move-remove") {
            const deltaX = e.clientX - this.initialX;
            const deltaY = e.clientY - this.initialY;
            const newLeft = this.startX + deltaX;
            const newTop = this.startY + deltaY;

            selectedIconElement.style.left = newLeft + "px";
            selectedIconElement.style.top = newTop + "px";

            // Save icon positions to local storage
            saveIconPositions();
        }
    }

    // Event listener for the "Delete" key to remove the selected icon
    document.addEventListener("keydown", function (e) {
        if (e.key === "Delete" && mode === "move-remove" && selectedIconElement) {
            selectedIconElement.remove();
            selectedIconElement = null;
            removeHighlight();
            saveIconPositions();
        }
    });
    function addIcon(e) {
        // Set selectedIcon to default if it's null
        if (selectedIcon === null) {
            selectedIcon = "img/house.png"; // Replace with the path to your default icon
        }
    
        const icon = createIcon(selectedIcon);
        villageContainer.appendChild(icon);
    
        // Adjust the position of the icon based on mouse click coordinates
        const rect = villageContainer.getBoundingClientRect();
        const offsetX = e.clientX - rect.left - icon.width / 2;
        const offsetY = e.clientY - rect.top - icon.height / 2;
    
        icon.style.left = offsetX + "px";
        icon.style.top = offsetY + "px";
    
        // Save icon positions to local storage
        saveIconPositions();
    }

    
    function createIcon(iconSrc) {
        const icon = document.createElement("img");
        icon.src = iconSrc;
        icon.className = "icon";
        icon.width = 50; // Adjust the icon size as needed
        icon.height = 50;

        // Add event listeners for icon interactions
        icon.addEventListener("mousedown", handleIconMouseDown);
        icon.addEventListener("mousemove", handleIconMouseMove);
        icon.addEventListener("mouseup", handleIconMouseUp);

        return icon;
    }

    function handleIconMouseDown(e) {
        e.preventDefault();
        if (mode === "place") return; // Skip if in place mode
        this.dragging = true;
        this.initialX = e.clientX;
        this.initialY = e.clientY;
        this.startX = parseInt(this.style.left, 10) || 0;
        this.startY = parseInt(this.style.top, 10) || 0;
    }

    function handleIconMouseMove(e) {
        if (this.dragging && mode === "move-remove") {
            const deltaX = e.clientX - this.initialX;
            const deltaY = e.clientY - this.initialY;
            const newLeft = this.startX + deltaX;
            const newTop = this.startY + deltaY;

            selectedIconElement.style.left = newLeft + "px";
            selectedIconElement.style.top = newTop + "px";
        }
    }
    function handleIconMouseUp() {
        this.dragging = false;
        saveIconPositions();
    }


    function saveIconPositions() {
        // Get all icon positions and save them to local storage
        const icons = document.querySelectorAll(".icon");
        const positions = Array.from(icons).map(icon => ({
            left: icon.style.left,
            top: icon.style.top
        }));
        localStorage.setItem("iconPositions", JSON.stringify(positions));
    }

    function loadIconPositions() {
        // Load icon positions from local storage and place icons accordingly
        const savedPositions = localStorage.getItem("iconPositions");
        if (savedPositions) {
            const positions = JSON.parse(savedPositions);

            // Clear existing icons and reattach event listeners
            const existingIcons = document.querySelectorAll(".icon");
            existingIcons.forEach(icon => {
                icon.removeEventListener("mousedown", handleIconMouseDown);
                icon.removeEventListener("mousemove", handleIconMouseMove);
                icon.removeEventListener("mouseup", handleIconMouseUp);
            });

            // Add icons from saved positions and attach event listeners
            positions.forEach(position => {
                const icon = createIcon(selectedIcon);
                villageContainer.appendChild(icon);
                icon.style.left = position.left;
                icon.style.top = position.top;

                // Attach event listeners to all icons, including existing ones
                icon.addEventListener("mousedown", handleIconMouseDown);
                icon.addEventListener("mousemove", handleIconMouseMove);
                icon.addEventListener("mouseup", handleIconMouseUp);
            });

            // Save icon positions to local storage after loading
            saveIconPositions();
        }
    }
    function exportAsImage() {
        const container = document.getElementById("village-container");
        const icons = container.querySelectorAll(".icon");

        // Temporarily remove the background color from the icon class
        icons.forEach(icon => {
            icon.style.backgroundColor = "transparent";
        });

        html2canvas(container).then(canvas => {
            // Restore the original background color for the icons
            icons.forEach(icon => {
                icon.style.backgroundColor = "#3498db"; // Replace with your original background color
            });

            const imageData = canvas.toDataURL("image/png");
            const link = document.createElement("a");
            link.href = imageData;
            link.download = "village_image.png";
            link.click();
        });
    }


    function exportToJSON() {
        const icons = document.querySelectorAll(".icon");
        const positions = Array.from(icons).map(icon => ({
            left: icon.style.left,
            top: icon.style.top,
            src: icon.src
        }));
        const backgroundImage = villageContainer.style.backgroundImage;

        const jsonContent = JSON.stringify({
            backgroundImage,
            positions
        });

        const blob = new Blob([jsonContent], { type: "application/json" });
        const a = document.createElement("a");
        a.href = URL.createObjectURL(blob);
        a.download = "village_data.json";
        a.click();
    }

    // Function to import the village-container state from JSON
    function importFromJSON(file) {
        const reader = new FileReader();

        reader.onload = function (event) {
            const jsonContent = event.target.result;
            try {
                const data = JSON.parse(jsonContent);

                // Clear existing icons and reattach event listeners
                const existingIcons = document.querySelectorAll(".icon");
                existingIcons.forEach(icon => {
                    icon.removeEventListener("mousedown", handleIconMouseDown);
                    icon.removeEventListener("mousemove", handleIconMouseMove);
                    icon.removeEventListener("mouseup", handleIconMouseUp);
                });

                // Set the background image
                villageContainer.style.backgroundImage = data.backgroundImage;

                // Add icons from imported positions and attach event listeners
                data.positions.forEach(position => {
                    const icon = createIcon(position.src);
                    villageContainer.appendChild(icon);
                    icon.style.left = position.left;
                    icon.style.top = position.top;

                    // Attach event listeners to all icons, including existing ones
                    icon.addEventListener("mousedown", handleIconMouseDown);
                    icon.addEventListener("mousemove", handleIconMouseMove);
                    icon.addEventListener("mouseup", handleIconMouseUp);
                });

                // Save icon positions to local storage after importing
                saveIconPositions();
            } catch (error) {
                console.error("Error parsing JSON:", error);
            }
        };

        reader.readAsText(file);
    }

    // Event listener for the "Save Image" button
    const saveButton = document.getElementById("save-image");
    saveButton.addEventListener("click", exportAsImage);

    // Event listener for the "Load Icons" button
    const loadButton = document.getElementById("load-json");
    loadButton.addEventListener("click", loadIconPositions);

    // Event listener for the "Export to JSON" button
    const exportJSONButton = document.getElementById("export-json-button");
    exportJSONButton.addEventListener("click", exportToJSON);

    // Event listener for the "Import JSON" input
    const importInput = document.getElementById("load-json");
    importInput.addEventListener("change", function (e) {
        const file = e.target.files[0];
        if (file) {
            importFromJSON(file);
        }
    });
    // Dynamically populate the select menu with icons
    const iconDirectory = "img/"; // Adjust the directory path as needed

});
